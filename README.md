# TuneWorld

- Adrian Papania
- Diane Ancheril
- Daniel Berk
- Sam Alboher

## Design

- API Design [Relative document](docs/apis.md)
- Data Model [Relative document](docs/data-model.md)
- GHI [Relative document](docs/ghi.md)
- Integrations [Relative document](docs/integrations.md)

## Intended Market

Anyone interested in discovering new music and saving the songs/artists/albums that they like

## Functionality

- Ability to sign up/login
- Search for artists/albums/songs
- Liked pages showing your liked items
- Add artists/albums/songs to your liked pages
- Remove artists/albums/songs from your liked pages
- Navigate to an artist's Spotify page from your liked artists
- Able to add a bio and a picture to your account
- See all the songs that are in a liked album
- Preview links to listen to the liked songs

### Project Initialization

To use this application on your local machine:

1. Clone the repository down to your local machine
2. cd into the new project directory
3. Create a .env file in the top directory
4. Go to https://developer.spotify.com/dashboard/
5. Sign up/ sign in with a Spotify account
6. Create App in Spotify Developer Dashboard
7. Follow exampleenv.md and fill in the CLIENT_ID and CLIENT_SECRET from the application's dashboard
8. Generate a 256bit SIGNING_KEY from https://www.allkeysgenerator.com/Random/Security-Encryption-Key-Generator.aspx
9. Fill in the .env SIGNING_KEY
10. Run `docker volume create postgres-data`
11. Run `docker compose build`
12. Run `docker compose up`
13. Open http://localhost:3000/ in your browser

### Stretch Goals
- Ability to search for other user's profiles
- Contact other users using mailhog

### Test

1. Test for album POST route [Relative document](tuneworldapi/tests/test_album_routes.py) - Adrian
2. Test for artist POST route [Relative document](tuneworldapi/tests/test_artist_routes.py) - Dan
3. Test for song POST route [Relative document](tuneworldapi/tests/test_song_routes.py) - Diane
4. Test for song GET route [Relative document](tuneworldapi/tests/test_song_routes.py) - Sam
