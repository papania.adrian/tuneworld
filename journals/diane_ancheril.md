Jan 4, 2023
Dan forked and cloned the project, completed the project setup instructions and added the rest of us as maintainers.
Sam completed the pgadmin setup.
I then cloned the project on to my system and set up the containers in Docker.

Jan 5, 2023
Dan and Sam worked on setting up the authentication files.
Adrian and I created a Song table and set up the endpoint for creating a song.

Jan 6, 2023
Dan and Adrian worked on seeting up more tables in the database. Sam and I went through documentation and videos on how to incorporate Spotify through a third-party api (Spotipy).

Jan 9, 2023
We parsed through data from the Spotify third-party api and set up an endpoint to get all the albums when searching for an artist, and an endpoint to access all the songs in an album.

Jan 10, 2023
Spent some time reading up on React and Redux

Jan 11, 2023
We spent almost the entire day trying to figure out how to setup the frontend authentication.

Jan 12, 2023
We all spent some time doing some research on how to design the frontend. Sam and I got the authentication to work with the signup form on the front end. I also added an artist field to the albums table and created a get_all function to get a list of all the users.

Jan 13, 2023
Started working on the liked albums, liked songs and liked artists functionality.

Jan 17, 2023
I set up the liked albums in a way so that when clicked, it shows the tracks of the album in a table. I modified the get_all_tracks_album_uri() so that we get the artist name, album name, preview url and album artwork for each track.

Jan 18, 2023
Set up the liked artist page so that when you click on an artist card it redirects to the artist's Spotify page.

Jan 19, 2023
Started merging css with the liked functionality. We spent all day trying to figure out how to get the tracks to show in a table beside the album artwork when the page loads.

Jan 20, 2023
We finally figured out yesterday's table issue. We set up the tracks for an album to be a separate component which gets called in the liked albums component so that it renders for each liked album.

Jan 23, 2023
We merged Sam's styling with Dan's search functionality. We also worked on linking the liked pages to render based on the logged in account. It was exciting to see the app slowly coming to life! Deleted my branch after sending all of my work to Sam and created a new one with all functionality and styling in place.

Jan 24, 2023
We set up the log out and log in buttons to show based on whether the user is authenticated or not. We also started working on our unit tests.

Jan 25, 2023
We protected all of our endpoints and components today. The authentication process led to a lot of errors which we managed to fix by the end of the day.

Jan 26, 2023
We tried to fix an issue with the update profile. The state was not being set when a user inputs a value in the update form. Also corrected the math to show the correct value for the duration of a song. Deleted unused imports and comments that were not required. Also started cleaning up our code using Black.
