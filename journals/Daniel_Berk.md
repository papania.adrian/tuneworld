## January 4, 2023

Today, I worked on:

- Setting up the project

Created a group for our team in gitlab. Forked and cloned the project. Completed all of the project setup steps.

## January 5, 2023

Today, I worked on:

-Authorization/account table

Sam and I created the account table and started working on the authorization and creating accounts. We had several problems, but using print statements and looking through the jwtdown-fastapi source code we were able to figure out our biggest problems.

## January 6, 2023

Today, I worked on:

-Authorization/tables

Finished up authorization with Adrian, created all endpoints for listing, getting, deleting, and update songs.

## January 9, 2023

Today, I worked on:

-Creating tables/ Functions for parsing through Spotify Api's data

We all came together and worked on searching and parsing through the Spotify Api's data. We then used that to create functions as endpoints for getting album information based on a search for an artist, and then all the tracks in an album based on its URI.

## January 10, 2023

Today, I worked on:

-Fixed problems with tables/updated tables

## January 11, 2023

Today, I worked on:

-FastAPI endpoints

Created endpoints for albums and artists, still need to decide on how we want the songs table to be and then update those endpoints.


## January 12-13, 2023

Today, I worked on:

-Search page

Created a react page for searching for albums by the artist's name. Still need to add more functionality to it, and then edit the page to match our design.

## January 17, 2023

Today, I worked on:

-Search page

Added Artist/album/track dropdown and functionality. Had several issues, one thing that helped fix some was learning more about the ternary operator. Eventually figured out my issues with changing between tracks and album/artist in the dropdown.

## January 18, 2023

Today, I worked on:

-Search page

Added the like functionality, both front-end and back-end for albums. Tomorrow I will add the same for tracks and artists.

## January 19, 2023

Today, I worked on:

-Search page, tables, songs/artist queries and routers

Added the like functionality for artists and songs, had to change the tables and queries/routers to represent the proper data.

## January 20, 2023

Today, I worked on:

-Search page, album table/queries/router

Finally figured out how to show which albums are liked based on which account is logged in. Kept trying to use .filter() and it was not working how I thought it would, eventually changed the get_all() endpoint to filter by the account_id.

## January 22, 2023

Today, I worked on:

-Search page, artist and songs tables/queries/routers

Added functionality for artists and songs to show if they are liked only by the logged in account. Used the same fix as I did on January 20 and it worked well. All functionality for the search page should be done, just need to clean up the code, add a test, and edit code for CI/CD.

## January 23, 2023

Today, I worked on:

-Liked artist/albums/songs pages, search page

Diane and I got the liked artists/albums/songs pages to properly pull and show data for each. I editted the search page to not show the heart unless you are signed in, so you cannot like a song unless you are signed in.

## January 24, 2023

Today, I worked on:

-Front-end Logged in vs logged out

We worked together as a team and got the front-end loading differently for logged in vs logged out.

## January 25, 2023

Today, I worked on:

-Authentication, Documentation

Added the requiring valid token to the back-end endpoints and added the appropriate functions on the search page front-end. Created all the documentation for the project so far.

## January 26, 2023

Today, I worked on:

-Miscellanious errors/issues

We went through the project and kept testing out all of the features to make sure that they work how we want them to. We had issues with editting the account information, some errors with using the search page without being logged in, setup our sign up form to give a default picture url, and some more errors throughout the project. Everything looks to be working well, just need to do CI/CD and deploy.

## January 27, 2023

Today, I worked on:

-Final Touches, CI/CD

We all worked together and polished our website and finished the CI/CD and we started working on deployment.
