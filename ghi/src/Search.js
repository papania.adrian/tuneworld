import { React, useState, useEffect } from "react";
import { InputGroup, FormControl, Button, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import music_pic from "./images/music.png";
import Heart from "react-heart";
import "./Artist/ArtistProfile.css";
import { useAuthContext } from "./useToken";
import "./nav.css";

const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;
const CLIENT_SECRET = process.env.REACT_APP_CLIENT_SECRET;

function Search() {
  const [searchInput, setSearchInput] = useState("");
  const [accessToken, setAccessToken] = useState("");
  const [albums, setAlbums] = useState([]);
  const [searchValue, setSearchValue] = useState("artist");
  const [stateAlbums, setStateAlbums] = useState([]);
  const [stateArtists, setStateArtists] = useState([]);
  const [stateTracks, setStateTracks] = useState([]);
  const { token } = useAuthContext();

  useEffect(() => {
    // API Access Token
    var authParameters = {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body:
        "grant_type=client_credentials&client_id=" +
        CLIENT_ID +
        "&client_secret=" +
        CLIENT_SECRET,
    };
    fetch("https://accounts.spotify.com/api/token", authParameters)
      .then((result) => result.json())
      .then((data) => setAccessToken(data.access_token));
  }, []);

  // Search for artist
  async function searchArtists() {
    const searchParameters = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + accessToken,
      },
    };
    await fetch(
      "https://api.spotify.com/v1/search?q=" + searchInput + "&type=artist",
      searchParameters
    )
      .then((response) => response.json())
      .then((data) => setAlbums(data.artists.items));
    fetchArtistDB();
  }

  //Search for albums
  async function searchAlbums() {
    const searchParameters = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + accessToken,
      },
    };
    await fetch(
      "https://api.spotify.com/v1/search?q=" + searchInput + "&type=album",
      searchParameters
    )
      .then((response) => response.json())
      .then((data) => setAlbums(data.albums.items));
    fetchAlbumDB();
  }

  // Search for tracks
  async function searchTracks() {
    const searchParameters = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + accessToken,
      },
    };
    await fetch(
      "https://api.spotify.com/v1/search?q=" + searchInput + "&type=track",
      searchParameters
    )
      .then((response) => response.json())
      .then((data) => {
        setAlbums(data.tracks.items);
      });
    fetchTrackDB();
  }

  function clearSearch(event) {
    setSearchValue(event.target.value.toLowerCase());
    setAlbums([]);
  }

  async function handleLikeAlbum(event) {
    let response;
    if (stateAlbums.includes(event.uri)) {
      response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/albums/${event.uri}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: {
            uri: event.uri,
          },
        }
      );
    } else {
      response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/albums`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(event),
        }
      );
    }
    if (response.ok) {
      fetchAlbumDB();
    }
  }

  async function fetchAlbumDB() {
    if (token) {
      const response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/albums`,
        {
          method: "GET",
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      const data = await response.json();
      setStateAlbums(data.map((album) => album.uri));
    }
  }

  async function handleLikeArtist(event) {
    let response;
    if (stateArtists.includes(event.uri)) {
      response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/artists/${event.uri}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: {
            uri: event.uri,
          },
        }
      );
    } else {
      response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/artists`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(event),
        }
      );
    }
    if (response.ok) {
      fetchArtistDB();
    }
  }

  async function fetchArtistDB() {
    if (token) {
      const response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/artists`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      const data = await response.json();
      setStateArtists(data.map((artist) => artist.uri));
    }
  }

  async function handleLikeTrack(event) {
    let response;
    if (stateTracks.includes(event.uri)) {
      response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/songs/${event.uri}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: {
            uri: event.uri,
          },
        }
      );
    } else {
      response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/songs`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },

          body: JSON.stringify(event),
        }
      );
    }
    if (response.ok) {
      fetchTrackDB();
    }
  }
  async function fetchTrackDB() {
    if (token) {
      const response = await fetch(
        `${process.env.REACT_APP_TUNEWORLD_API_HOST}/songs`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      const data = await response.json();
      setStateTracks(data.map((tracks) => tracks.uri));
    }
  }

  return (
    <>
      <div>
        <div>
          <InputGroup className="mb-3" size="lg">
            <Form.Select
              onChange={(event) => clearSearch(event)}
              aria-label="Default select example"
            >
              <option>Artist</option>
              <option>Album</option>
              <option>Track</option>
            </Form.Select>
            <FormControl
              placeholder="Search For"
              type="input"
              onKeyPress={(event) => {
                if (event.key === "Enter") {
                  if (searchValue === "artist") {
                    searchArtists();
                  } else if (searchValue === "album") {
                    searchAlbums();
                  } else {
                    searchTracks();
                  }
                }
              }}
              onChange={(event) => setSearchInput(event.target.value)}
            />

            <Button
              className="search"
              onClick={(event) => {
                if (searchValue === "artist") {
                  searchArtists();
                } else if (searchValue === "album") {
                  searchAlbums();
                } else {
                  searchTracks();
                }
              }}
            >
              Search
            </Button>
          </InputGroup>
        </div>
      </div>
      <div className="subGrid">
        {albums.map((data, i) => {
          if (searchValue === "album") {
            if (!token) {
              return (
                <div key={i}>
                  <div className="albumCard">
                    <div className="albumContainer" align="center">
                      <div align="center">
                        <img
                          src={data.images[0] ? data.images[0].url : music_pic}
                          alt="Album"
                        />
                      </div>
                      <h4 align="center">
                        <b>{data.name}</b>
                      </h4>
                      <p align="center">
                        {data.artists ? data.artists[0].name : null}
                      </p>
                    </div>
                  </div>
                </div>
              );
            } else {
              return (
                <div key={i}>
                  <div className="albumCard">
                    <div className="albumContainer" align="center">
                      <div align="center">
                        <img
                          src={data.images[0] ? data.images[0].url : music_pic}
                          alt="Album"
                        />
                      </div>
                      <h4 align="center">
                        <b>{data.name}</b>
                      </h4>
                      <p align="center">
                        {data.artists ? data.artists[0].name : null}
                      </p>
                      <div style={{ width: "2rem" }}>
                        <Heart
                          key={i}
                          onClick={() =>
                            handleLikeAlbum({
                              uri: data.uri,
                              name: data.name,
                              picture_url: data.images[0]
                                ? data.images[0].url
                                : music_pic,
                              artist: data.artists[0].name,
                            })
                          }
                          isActive={stateAlbums.includes(data.uri)}
                          animationScale={1.25}
                          style={{ marginBottom: "1rem" }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              );
            }
          } else if (searchValue === "artist") {
            if (!token) {
              return (
                <div key={i}>
                  <div className="albumCard">
                    <div className="albumContainer" align="center">
                      <div align="center">
                        <img
                          src={data.images[0] ? data.images[0].url : music_pic}
                          alt="Artist"
                        />
                      </div>
                      <h4 align="center">
                        <b>{data.name}</b>
                      </h4>
                      <p align="center">
                        Followers:{" "}
                        {data.followers ? data.followers.total : null}
                      </p>
                    </div>
                  </div>
                </div>
              );
            } else {
              return (
                <div key={i}>
                  <div className="albumCard">
                    <div className="albumContainer" align="center">
                      <div align="center">
                        <img
                          src={data.images[0] ? data.images[0].url : music_pic}
                          alt="Artist"
                        />
                      </div>
                      <h4 align="center">
                        <b>{data.name}</b>
                      </h4>
                      <p align="center">
                        Followers:{" "}
                        {data.followers ? data.followers.total : null}
                      </p>
                      <div style={{ width: "2rem" }}>
                        <Heart
                          key={i}
                          onClick={() =>
                            handleLikeArtist({
                              name: data.name,
                              uri: data.uri,
                              picture_url: data.images[0]
                                ? data.images[0].url
                                : music_pic,
                            })
                          }
                          isActive={stateArtists.includes(data.uri)}
                          animationScale={1.25}
                          style={{ marginBottom: "1rem" }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              );
            }
          } else {
            if (!token) {
              return (
                <div key={i}>
                  <div className="albumCard">
                    <div className="albumContainer" align="center">
                      <div align="center">
                        <img
                          src={
                            data.album ? data.album.images[0].url : music_pic
                          }
                          alt="Track"
                        />
                      </div>
                      <h4 align="center">
                        <b>{data.name}</b>
                      </h4>
                      <p align="center">
                        {data.artists ? data.artists[0].name : null}
                      </p>
                    </div>
                  </div>
                </div>
              );
            } else {
              return (
                <div key={i}>
                  <div className="albumCard">
                    <div className="albumContainer" align="center">
                      <div align="center">
                        <img
                          src={
                            data.album ? data.album.images[0].url : music_pic
                          }
                          alt="Track"
                        />
                      </div>
                      <h4 align="center">
                        <b>{data.name}</b>
                      </h4>
                      <p align="center">
                        {data.artists ? data.artists[0].name : null}
                      </p>
                      <div style={{ width: "2rem" }}>
                        <Heart
                          key={i}
                          onClick={() =>
                            handleLikeTrack({
                              uri: data.uri,
                              title: data.name,
                              time: data.duration_ms,
                              artist: data.artists ? data.artists[0].name : "",
                              album:
                                data.album.album_type === "album"
                                  ? data.album.name
                                  : "single",
                              preview_url: data.preview_url
                                ? data.preview_url
                                : "",
                            })
                          }
                          isActive={stateTracks.includes(data.uri)}
                          animationScale={1.25}
                          style={{ marginBottom: "1rem" }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              );
            }
          }
        })}
      </div>
    </>
  );
}
export default Search;
