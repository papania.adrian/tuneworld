import { useState} from 'react';
import { useToken } from "./useToken";
import { useNavigate } from 'react-router-dom';
import tuneworld_logo from "./images/music.png";

function SignUpComponent() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [full_name, setFull_Name] = useState('');
    const [bio, setBio] = useState('');
    const [picture_url, setPicture_Url] = useState(tuneworld_logo)
    const [token, login] = useToken(); // eslint-disable-line no-use-before-define
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
    e.preventDefault();
    try {
    const url = `${process.env.REACT_APP_TUNEWORLD_API_HOST}/api/accounts`;
    const response = await fetch(url, {
      method: "post",
      body: JSON.stringify({
        password,
        email,
        full_name,
        bio,
        picture_url
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      await login(email, password);
      setEmail('')
      setPassword('')
      setFull_Name('')
      setPicture_Url('')
      setBio('')
      navigate('/');
      window.location.reload(false);

    }
    return false;

    } catch (error) {
      console.log(error);
      console.log(token)
      console.log("Wrong email or password!");
    }
  }


    return (
      <div className="positioning">
        <div className="container2">
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label style={{ color: "#E23E57" }} htmlFor="email" className="form-label">Email address</label>
                <input placeholder="Email" onChange={(e) => setEmail(e.target.value)} value={email}  type="email" className="form-control" id="email"/>
            </div>
            <div className="mb-3">
                <label style={{ color: "#E23E57" }} htmlFor="password" className="form-label">Password</label>
                <input placeholder="Password" onChange={(e) => setPassword(e.target.value)} value={password}  type="password" className="form-control" id="password"/>
            </div>
            <div className="mb-3">
                <label style={{ color: "#E23E57" }} htmlFor="full_name" className="form-label">Name</label>
                <input placeholder="Full name(First Last)" onChange={(e) => setFull_Name(e.target.value)} value={full_name}  type="text" className="form-control" id="full_name"/>
            </div>
            <div className="mb-3">
                <label style={{ color: "#E23E57" }} htmlFor="bio" className="form-label">Bio</label>
                <textarea placeholder="Bio(optional)"  onChange={(e) => setBio(e.target.value)} value={bio}  type="text" className="form-control" id="bio"/>
            </div>
                  <button
        className="btn btn-outline-light my-2 my-sm-0"
        type="submit"
        style={{ color: "#E23E57" }}
      >
        Submit
      </button>
        </form>
      </div>
      </div>
    )

}
export default SignUpComponent
