import './ArtistProfile.css'
import { useEffect, useState } from "react";
import { useAuthContext } from "../useToken";

function UpdateComponent() {
  const { token } = useAuthContext();
  const [bio, setBio] = useState("");
  const [picture_url, setPicture_Url] = useState("");

  useEffect(() => {

    async function getAccount(){
      if (token) {
        try {
          const url = `${process.env.REACT_APP_TUNEWORLD_API_HOST}/api/accounts/id`;
          const response = await fetch(url, {
            method: "GET",
            headers: {
              Authorization: `Bearer ${token}`
            },
            // credentials:"include"
          });
          const data = await response.json();
          setPicture_Url(data.picture_url)
          setBio(data.bio)
        } catch (error) {
          console.log(error);
        }
      }
    }
    getAccount()
  }, [token]);

  const handleUpdate = async (e) => {
    e.preventDefault();
    try {
      const url = `${process.env.REACT_APP_TUNEWORLD_API_HOST}/api/accounts`;
      const response = await fetch(url, {
        method: "PUT",
        body: JSON.stringify({
          bio: bio,
          picture_url: picture_url,
        }),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.ok) {
        setBio("");
        setPicture_Url("");
        window.location.reload(false)
      }
      return false;
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="positioning">
      <div className="container2">
        <form onSubmit={handleUpdate}>
          <div className="mb-3">
            <label
              style={{ color: "#E23E57" }}
              htmlFor="picture_url"
              className="form-label"
            >
              Picture_Url
            </label>
            <input
              onChange={(e) => setPicture_Url(e.target.value)}
              value={picture_url}
              type="text"
              className="form-control"
              id="picture_url"
            />
          </div>
          <div className="mb-3">
            <label
              style={{ color: "#E23E57" }}
              htmlFor="bio"
              className="form-label"
            >
              Bio
            </label>
            <textarea
              onChange={(e) => setBio(e.target.value)}
              value={bio}
              type="text"
              className="form-control"
              id="bio"
            />
          </div>
          <button
            className="btn btn-outline-light my-2 my-sm-0"
            type="submit"
            style={{ color: "#E23E57" }}
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}
export default UpdateComponent;
