import React, { useState, useEffect } from "react";
import "./Table.css";
import "./Artist/ArtistProfile.css";

function LikedAlbumsTable(props) {
  const [tracks, setTracks] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      let u = props.uri;
      try {
        const response = await fetch(
          `${process.env.REACT_APP_TUNEWORLD_API_HOST}/api/spotify/album/${u}`
        );
        const data = await response.json();
        setTracks(data.track_info);
      } catch (e) {
        console.log(e)
      }
    };
    fetchData();
  }, [props]);

  return (
    <table id="songs">
      <thead id="albumPosition">
        <tr >
          <th>Title</th>
          <th>Duration</th>
        </tr>
      </thead>
      <tbody>
        {tracks?.map((track) => (
          <tr key={track.id}>
            <td>
              <a id="title" href={track.preview}>{track.title}</a>
            </td>
            <td>{track.duration}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
export default LikedAlbumsTable;
