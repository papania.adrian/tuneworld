import React, { useState, useEffect } from "react";
import "./Artist/ArtistProfile.css";
import "./LikedAlbum.css";
import LikedAlbumsTable from "./LikedAlbumsTable";
import { useToken } from "./useToken";

function LikedAlbums() {
  const [albums, setAlbums] = useState([]);
  const [token] = useToken();

  useEffect(() => {
    async function fetchData() {
      try {
        await new Promise((r) => setTimeout(r, 1000));
        const response = await fetch(
          `${process.env.REACT_APP_TUNEWORLD_API_HOST}/albums`,
          {
            method: "GET",
            headers: {
              Authorization: `Bearer ${token}`,
            },
            credentials: "include",
          }
        );
        const data = await response.json();
        setAlbums(data);
      } catch (error) {
        console.log(error);
      }
    }
    if (token) {
      fetchData();
    }
  }, [token]);


  return (
    <>
      {albums.map((album, i) => {
        return (
          <div key={i}>
            <div className="likedAlbumGrid">
              <div>
                <div key={album.id} className="likedAlbumCard">
                  <img
                    className="card-img-top"
                    src={album.picture_url}
                    alt="Album artwork"
                  />
                  <div className="card-body">
                    <h4 className="card-title" style={{ color: "#E23E57" }}>
                      {album.name}
                    </h4>
                    <p className="card-text" style={{ color: "#E23E57" }}>
                      {album.artist}
                    </p>
                  </div>
                </div>
              </div>
              <div className="gridComponentTable">
                <div className="tracks"></div>
                <LikedAlbumsTable uri={album.uri} />
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
}
export default LikedAlbums;
