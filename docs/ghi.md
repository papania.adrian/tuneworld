## Customer Graphical Human Interface

# Main page (not logged in)

This will be the first page that visitors arrive to. They will be able to search for songs/artists/albums and be able to signup or login.

![MainPageSO](wireframes/main-page-signed-out.png)

# Main page (logged in)

This will show when a user is signed in. They will have access to their profile and able to search and like songs/artists/albums.

![MainPageSI](wireframes/main-page-signed-in.png)

# Login Form

This page will show when the login button is clicked and log in the user.

![Login](wireframes/login-form.png)

# Signup Form

This page will show when the signup button is clicked and will signup and login a user.

![Signup](wireframes/signup-form.png)

# Profile Main Page

This page will show when a signed in user clicks on the profile button in the nav bar. It will allow the user to edit their profile, view their profile, and click the favorites button.

![ProfileMP](wireframes/profile-main-page.png)

# Profile Favorited Artists

This page will show up when a signed in user clicks on their profile and then the My Favorites tab. It will default show the favorited artists and will show buttons to view the favorite albums and songs pages.

![ProfileFA](wireframes/profile-favorited-artists.png)

# Profile Favorited Albums

This page will show up when you click the albums button and will show the user's liked albums as well as the songs in the album. If you click on the song title it will play a preview of the song.

![ProfileFAL](wireframes/profile-favorited-albums.png)

# Profile Favorited Songs

This page will show up when you click the songs button and will show the user's liked songs in a table. Each row will show a songs title, artist, album name or single, duration, and a link to the spotify preview.

![ProfileFS](wireframes/profile-favorited-songs.png)
