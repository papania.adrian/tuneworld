from fastapi.testclient import TestClient
from queries.artists import ArtistRepository, ArtistIn, ArtistOut
from authenticator import authenticator
from main import app
import json

client = TestClient(app=app)


def get_current_account_data_mock():
    return {"id": 30}


class ArtistRepositoryMock:
    def create(self, artist: ArtistIn) -> ArtistOut:
        artist_dict = artist.dict()
        return ArtistOut(id=2, **artist_dict)


def test_create_artist():

    app.dependency_overrides[ArtistRepository] = ArtistRepositoryMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock
    artist_body = {
        "name": "Hayley Williams",
        "uri": "6Rx1JKzBrSzoKQtmbVmBnM",
        "picture_url":
        "https://i.scdn.co/image/ab676161000051745a00969a4698c3132a15fbb0",
        "account_id": 3,
    }

    response = client.post("/artists", json.dumps(artist_body))

    assert response.status_code == 200
    assert response.json()["id"] == 2
    assert response.json()["account_id"] == 30

    app.dependency_overrides = {}
