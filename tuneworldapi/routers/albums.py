from fastapi import APIRouter, Response, Depends, HTTPException
from queries.albums import AlbumIn, AlbumOut, Error, AlbumRepository
from typing import Union, List
from authenticator import authenticator

router = APIRouter()


@router.post("/albums", response_model=Union[AlbumOut, Error])
def create_album(
    album: AlbumIn,
    response: Response,
    repo: AlbumRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        album.account_id = account_data["id"]
        album = repo.create(album)
    except Exception:
        return {"message": "Could not create Album"}
    return album


@router.get("/albums/{name}", response_model=Union[AlbumOut, Error])
def get_album(
    name: str,
    repo: AlbumRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> AlbumOut:
    album = repo.get(name)
    if album is None:
        raise HTTPException(status_code=404, detail="Album not found")
    return album


@router.delete("/albums/{uri}", response_model=Union[bool, Error])
def delete_album(
    uri: str,
    repo: AlbumRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    account_id = account_data["id"]
    return repo.delete(uri, account_id)


@router.get("/albums", response_model=List[AlbumOut])
def get_all(
    repo: AlbumRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.get_all(account_id)
