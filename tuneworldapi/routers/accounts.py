from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from typing import List, Optional
from authenticator import authenticator

from pydantic import BaseModel

from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountQueries,
    DuplicateAccountError,
    AccountUpdateIn
)


class AccountForm(BaseModel):
    username: str
    password: str
    bio: str
    picture_url: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


@router.get("/api/accounts/id", response_model=AccountOut | HttpError)
async def get_account_by_id(
    response: Response,
    accounts: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    Account_id = account_data['id']
    if account_data is not None:
        return accounts.get_account_by_id(Account_id)
    else:
        response.status_code = 404


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(
        username=info.email,
        password=info.password,
        bio=info.bio,
        picture_url=info.picture_url,
    )
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.put("/api/accounts")
async def update_user(
    info: AccountUpdateIn,
    response: Response,
    accounts: AccountQueries = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.get_current_account_data
    ),
):
    if account_data:
        account_id = account_data["id"]
        return accounts.update_user(info, account_id)
    else:
        response.status_code = 404


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.get("/api/accounts", response_model=List[AccountOut])
def get_all(
    repo: AccountQueries = Depends(),
):
    return repo.get_all()
